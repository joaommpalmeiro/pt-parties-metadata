# pt-parties-metadata

Metadata about Portuguese political parties to reuse in different projects.

| File                                                                | Description                                                                                                                                                      | Source                                 |
| ------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| [2024-legislative-parties.json](data/2024-legislative-parties.json) | Names and abbreviations/acronyms according to the [2024 Portuguese legislative election](https://www.cne.pt/content/eleicoes-para-assembleia-da-republica-2024). | [Script](validate-csv-table/01.py)     |
| [2024-legislative.json](data/2024-legislative.json)                 | Parties by constituency for the [2024 Portuguese legislative election](https://www.cne.pt/content/eleicoes-para-assembleia-da-republica-2024).                   | [Script](validate-csv-table/01.py)     |
| [cne-parties.json](data/cne-parties.json)                           | Names and abbreviations/acronyms according to [CNE](https://www.cne.pt/).                                                                                        | [Script](extract-data-pdf-table/02.py) |
| [common-parties.json](data/common-parties.json)                     | Common names and abbreviations/acronyms to refer to certain political parties.                                                                                   | [Script](validate-csv-table/02.py)     |

## References

- https://www.cne.pt/sites/default/files/dl/pp_contactos/pp_contactos_partidos_inscritos_tc.pdf (_atualizado 29-01-2024_)
  - https://files.diariodarepublica.pt/2s/2009/08/159000000/3362833628.pdf
  - https://www.cne.pt/sites/default/files/dl/pp_pev_acordao_324_89.pdf
  - https://files.diariodarepublica.pt/2s/2019/07/138000000/0012900138.pdf
  - https://www.cne.pt/sites/default/files/dl/pp_adn_acordao_tc_765_2021.pdf
  - https://files.diariodarepublica.pt/2s/2014/11/213000000/2780427805.pdf
  - https://www.cne.pt/sites/default/files/dl/pp_be_pp_acordao_196_99.pdf
  - https://www.cne.pt/sites/default/files/dl/pp_nd_acordao_4_2024.pdf
  - https://files.diariodarepublica.pt/2s/2009/08/159000000/3362833628.pdf
  - https://www.cne.pt/sites/default/files/dl/pp_psd_acordao_343_86.pdf
  - https://partido-rir.pt/ + https://drive.google.com/file/d/1pIeP--o-6AKXbvbdnXtjZ6og_t_yG3fa/view
- https://www.cne.pt/content/partidos-politicos-1
- https://pt.wikipedia.org/wiki/Lista_de_partidos_pol%C3%ADticos_em_Portugal
- https://www.tribunalconstitucional.pt/tc/partidos.html
- https://www.parlamento.pt/DeputadoGP/Paginas/GruposParlamentaresI.aspx
- https://www.parlamento.pt/Parlamento/Paginas/GlossarioSiglasPartidarias.aspx
- https://www.publico.pt/interactivo/partidos
- `Bloco`: https://www.bloco.org/ + https://www.publico.pt/2024/01/05/politica/noticia/bloco-abre-porta-nova-geringonca-deixa-caderno-encargos-negociar-acordo-2075747
- `Os Verdes`: https://osverdes.pt/ + https://www.publico.pt/os-verdes
- `Volt`: https://voltportugal.org/
- `CDS`: https://www.cds.pt/
- https://www.cne.pt/content/eleicoes-para-assembleia-da-republica-2024
- https://www.cne.pt/sites/default/files/dl/eleicoes/2024_ar/lista_candidatos/ar2024_sorteio_candidaturas_para_boletim_de_voto.pdf (_atualizado em: 03.02.2024_)
- https://pt.wikipedia.org/wiki/Elei%C3%A7%C3%B5es_legislativas_portuguesas_de_2024
- https://en.wikipedia.org/wiki/2024_Portuguese_legislative_election
