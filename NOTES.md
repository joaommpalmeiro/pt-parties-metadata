# Notes

- https://en.wikipedia.org/wiki/2024_Portuguese_legislative_election
- https://adidas.gitbook.io/api-guidelines/general-guidelines/json
- https://jsonapi.org/recommendations/:
  - https://jsonapi.org/recommendations/#date-and-time-fields
  - https://www.w3.org/TR/NOTE-datetime
  - https://jsonapi.org/recommendations/#naming
  - https://jsonapi.org/examples/
- https://en.wikipedia.org/wiki/Acronym
- https://en.wikipedia.org/wiki/Abbreviation
- https://www.cne.pt/sites/default/files/dl/eleicoes/2024_ar/lista_candidatos/ar2024_sorteio_candidaturas_para_boletim_de_voto.pdf
- _Denominação_: `name.cne`
- _Sigla_: `abbreviation.cne`
- https://github.com/camelot-dev/camelot
- https://github.com/camelot-dev/excalibur
- https://github.com/pdfminer/pdfminer.six
- https://github.com/chezou/tabula-py

## Snippets

```json
{
  "name": {
    "cne": ""
  },
  "abbreviation": {
    "cne": ""
  }
}
```

```json
{
  "cne_abbreviation": "",
  "abbreviation": "",
  "name": ""
}
```
