from pathlib import Path

import pandas as pd

COLS = ["Círculos", "Sorteio", "Sigla", "Denominação"]
OUTPUT_FIELDS = ["constituency", "number", "abbreviation", "name"]

OUTPUT_FOLDER = Path("..") / "data"
OUTPUT_FILE = OUTPUT_FOLDER / "2024-legislative.json"
OUTPUT_FILE_PARTIES = OUTPUT_FOLDER / "2024-legislative-parties.json"

if __name__ == "__main__":
    OUTPUT_FOLDER.mkdir(parents=True, exist_ok=True)

    df = pd.read_csv("data.csv")

    print(df.shape, end="\n" * 2)

    for col in df.columns:
        print(df[col].value_counts(dropna=False, sort=False).sort_index(), end="\n" * 2)

    df = df.rename(columns=dict(zip(COLS, OUTPUT_FIELDS)))

    df.to_json(
        OUTPUT_FILE,
        force_ascii=False,
        index=False,
        orient="records",
        indent=None,
    )

    df = df.drop_duplicates(subset=OUTPUT_FIELDS[-2:])
    df = df.drop(columns=OUTPUT_FIELDS[:2])
    df = df.sort_values(by=OUTPUT_FIELDS[-2])

    df.to_json(
        OUTPUT_FILE_PARTIES,
        force_ascii=False,
        index=False,
        orient="records",
        indent=None,
    )

    # TODO: Change to message from mensagens
    print("Done! ✓")
