import json
from operator import itemgetter
from pathlib import Path

OUTPUT_FOLDER = Path("..") / "data"
OUTPUT_FILE = OUTPUT_FOLDER / "common-parties.json"

DATA = [
    {"cne_abbreviation": "ADN", "name": "Alternativa Democrática Nacional"},
    {"cne_abbreviation": "B.E.", "abbreviation": "BE"},
    {"cne_abbreviation": "B.E.", "name": "Bloco"},
    {"cne_abbreviation": "CDS-PP", "abbreviation": "CDS"},
    {"cne_abbreviation": "ND", "name": "Nova Direita"},
    {"cne_abbreviation": "PAN", "name": "Pessoas-Animais-Natureza"},
    {"cne_abbreviation": "PEV", "name": "Os Verdes"},
    {"cne_abbreviation": "PPD/PSD", "abbreviation": "PSD"},
    {"cne_abbreviation": "R.I.R.", "abbreviation": "RIR"},
    {"cne_abbreviation": "VP", "name": "Volt"},
]


# TODO: Remove and use the one in vanillatils
def write_json(data, output_path, add_newline=True):
    with open(output_path, "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=2)

        if add_newline:
            f.write("\n")


if __name__ == "__main__":
    OUTPUT_FOLDER.mkdir(parents=True, exist_ok=True)

    write_json(
        sorted(DATA, key=itemgetter("cne_abbreviation"), reverse=False), OUTPUT_FILE
    )

    # TODO: Change to message from mensagens
    print("Done! ✓")
