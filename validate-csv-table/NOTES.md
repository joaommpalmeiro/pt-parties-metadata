# Notes

- https://www.adobe.com/acrobat/online/pdf-to-excel.html + Google Sheets
- https://pypi.org/project/pandas/
- https://pt.wikipedia.org/wiki/Distrito_eleitoral
- https://en.wikipedia.org/wiki/Electoral_district
- https://github.com/pandas-dev/pandas/issues/36912
- as-dev/pandas/issues/36912#issuecomment-704265031

## Commands

```bash
pipenv install --skip-lock pandas==2.2.0 && pipenv install --dev --skip-lock ruff==0.1.11 codespell==2.2.6
```

```bash
pipenv --rm
```

```bash
pipenv run codespell --help
```
