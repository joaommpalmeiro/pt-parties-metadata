from pathlib import Path

import tabula

# atualizado 29-01-2024
PDF_PATH = "https://www.cne.pt/sites/default/files/dl/pp_contactos/pp_contactos_partidos_inscritos_tc.pdf"

COLS = ["Sigla", "Denominação"]
OUTPUT_FIELDS = ["abbreviation", "name"]

OUTPUT_FOLDER = Path("..") / "data"
OUTPUT_FILE = OUTPUT_FOLDER / "cne-parties.json"

if __name__ == "__main__":
    OUTPUT_FOLDER.mkdir(parents=True, exist_ok=True)

    # print(tabula.environment_info())

    dfs = tabula.read_pdf(
        PDF_PATH,
        stream=False,
        pages=1,
        multiple_tables=False,
        output_format="dataframe",
        pandas_options={"header": 1, "usecols": COLS},
    )

    df = dfs[0]
    # print(df)

    df.loc[df[COLS[0]] == "R.I.R", COLS[0]] = "R.I.R."
    df = df.rename(columns=dict(zip(COLS, OUTPUT_FIELDS)))

    df.to_json(
        OUTPUT_FILE,
        force_ascii=False,
        index=False,
        orient="records",
        indent=None,
        lines=False,
    )

    # TODO: Change to message from mensagens
    print("Done! ✓")
