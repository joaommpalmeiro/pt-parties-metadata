# Notes

- https://github.com/chezou/tabula-py
- https://pypi.org/project/tabula-py/
- https://stackoverflow.com/questions/25833613/safe-method-to-get-value-of-nested-dictionary
- https://en.wiktionary.org/wiki/common_name
- https://github.com/camelot-dev/camelot/issues/326 + https://github.com/camelot-dev/camelot/issues/286
- https://github.com/camelot-dev/camelot/blob/v0.11.0/setup.py:
  - `all`
  - `opencv-python>=3.4.2.17`
- https://camelot-py.readthedocs.io/en/master/user/install-deps.html
- https://ghostscript.readthedocs.io/en/latest/Install.html#installing-ghostscript-on-macos
- https://github.com/atlanhq/camelot/issues/282#issuecomment-1023243604
- https://github.com/atlanhq/camelot/issues/282#issuecomment-1041619156
- https://github.com/atlanhq/camelot/issues/282#issuecomment-1063026282
- https://github.com/atlanhq/camelot/issues/282#issuecomment-1339206836
- https://github.com/atlanhq/camelot/issues/282#issuecomment-1412552637
- https://unstructured.io/blog/how-to-process-pdf-in-python
- https://unstructured-io.github.io/unstructured/best_practices/table_extraction_pdf.html
- https://github.com/Unstructured-IO/unstructured
- https://unstructured-io.github.io/unstructured/installation/full_installation.html
- https://pdf2image.readthedocs.io/en/latest/installation.html
- https://unstructured-io.github.io/unstructured/introduction/overview.html#tables
- https://github.com/Unstructured-IO/unstructured/blob/0.12.3/unstructured/partition/lang.py#L16
- https://unstructured-io.github.io/unstructured/introduction/overview.html#converting-elements-to-dictionary-or-json
- https://unstructured-io.github.io/unstructured/apis/api_parameters.html#output-format
- https://github.com/Unstructured-IO/unstructured/blob/0.12.3/unstructured/partition/pdf.py#L129
- https://github.com/Unstructured-IO/unstructured/blob/0.12.3/unstructured/partition/pdf.py#L467
- https://github.com/tonyroberts/pdf2array
- https://www.adobe.com/acrobat/online/pdf-to-excel.html + Google Sheets

## Commands

```bash
pipenv install --skip-lock "tabula-py[jpype]==2.9.0" "unstructured[pdf]==0.12.3" && pipenv install --dev --skip-lock ruff==0.1.11 codespell==2.2.6
```

```bash
pipenv --rm
```

```bash
pipenv run codespell --help
```

```bash
brew install ghostscript tcl-tk
```

```bash
pipenv install --skip-lock "camelot-py[all]==0.11.0" opencv-python-headless==3.4.18.65
```
