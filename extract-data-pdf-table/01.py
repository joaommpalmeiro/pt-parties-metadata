import json
from pathlib import Path

import tabula

# atualizado 29-01-2024
PDF_PATH = "https://www.cne.pt/sites/default/files/dl/pp_contactos/pp_contactos_partidos_inscritos_tc.pdf"

COLS = ["Sigla", "Denominação"]
OUTPUT_FIELDS = ["abbreviation", "name"]
CNE_FIELD = "cne"
DEFAULT_FIELD = "common"

# OUTPUT_PATH = Path("cne.json")
OUTPUT_PATH = Path("..") / "data.json"


EXTRA_DATA = {
    "ADN": {OUTPUT_FIELDS[1]: {DEFAULT_FIELD: "Alternativa Democrática Nacional"}},
    "B.E.": {
        OUTPUT_FIELDS[0]: {DEFAULT_FIELD: "BE"},
        OUTPUT_FIELDS[1]: {DEFAULT_FIELD: "Bloco"},
    },
    "CDS-PP": {OUTPUT_FIELDS[0]: {DEFAULT_FIELD: "CDS"}},
    "ND": {OUTPUT_FIELDS[1]: {DEFAULT_FIELD: "Nova Direita"}},
    "PAN": {OUTPUT_FIELDS[1]: {DEFAULT_FIELD: "Pessoas-Animais-Natureza"}},
    "PEV": {OUTPUT_FIELDS[1]: {DEFAULT_FIELD: "Os Verdes"}},
    "PPD/PSD": {OUTPUT_FIELDS[0]: {DEFAULT_FIELD: "PSD"}},
    "R.I.R.": {OUTPUT_FIELDS[0]: {DEFAULT_FIELD: "RIR"}},
    "VP": {OUTPUT_FIELDS[1]: {DEFAULT_FIELD: "Volt"}},
}


# TODO: Remove and use the one in vanillatils
def write_json(data, output_path, add_newline=True):
    with open(output_path, "w", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=2)

        if add_newline:
            f.write("\n")


def generate_field(party, field_name, field_col):
    return {
        field_name: {
            CNE_FIELD: party[field_col],
            **EXTRA_DATA.get(party[COLS[0]], {}).get(
                # field_name, {DEFAULT_FIELD: party[field_col]}
                field_name,
                {},
            ),
        },
    }


def process_party(party):
    return {
        **generate_field(party, OUTPUT_FIELDS[1], COLS[1]),
        **generate_field(party, OUTPUT_FIELDS[0], COLS[0]),
    }


if __name__ == "__main__":
    # print(tabula.environment_info())

    dfs = tabula.read_pdf(
        PDF_PATH,
        stream=False,
        pages=1,
        multiple_tables=False,
        output_format="dataframe",
        pandas_options={"header": 1, "usecols": COLS},
    )

    df = dfs[0]
    # print(df)

    df.loc[df[COLS[0]] == "R.I.R", COLS[0]] = "R.I.R."

    # df.to_json(OUTPUT_PATH, force_ascii=False, index=False, orient="records", indent=2)

    output = [process_party(party) for party in df.to_dict(orient="records")]
    # print(output)

    write_json(output, OUTPUT_PATH)

    # TODO: Change to message from mensagens
    print("Done! ✓")
