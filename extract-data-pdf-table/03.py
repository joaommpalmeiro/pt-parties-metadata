from pathlib import Path

from unstructured.partition.pdf import partition_pdf

# atualizado em: 03.02.2024
PDF_PATH = Path("ar2024_sorteio_candidaturas_para_boletim_de_voto.pdf")


if __name__ == "__main__":
    elements = partition_pdf(
        filename=PDF_PATH,
        infer_table_structure=True,
        strategy="hi_res",
        languages=["por"],
    )

    tables = [el for el in elements if el.category == "Table"]

    # print(tables[0].text)
    # print(tables[0].metadata.text_as_html)

    for table in tables:
        print(table.text, end="\n" * 2)

    print(len(tables))

    # from unstructured.staging.base import convert_to_dict
    # print(convert_to_dict(tables))
